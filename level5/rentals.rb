class Rental

	attr_accessor :id, :car_id, :time, :distance, :price, :start_date, :end_date,
				  :insurance_fee, :assistance_fee, :drivy_fee, :deductible_reduction,
				  :actions, :commission

	def initialize(id, car_id, distance, start_date, end_date, deductible_reduction)
		@id = id
		@car_id = car_id
		@distance = distance
		@start_date = start_date
		@end_date = end_date
		@deductible_reduction = deductible_reduction
		@time = getTime
	end

	def getTime
		(Time.parse(self.end_date).to_i - Time.parse(self.start_date).to_i) / 24 / 3600 + 1
	end

	def getPrice(cars)
		cars.each do |c|
			if self.car_id == c["id"]
				@price = getPricePerDayWithDecrease(c["price_per_day"]).round(0) + self.distance.to_i * c["price_per_km"]
			end
		end
	end

	def getCommissions
		@commission = self.price * 0.3
		self.insurance_fee = (commission * 0.5).round(0)
		self.assistance_fee = (self.time * 100).round(0)
		self.drivy_fee = (commission - self.insurance_fee - self.assistance_fee).round(0)
	end

	def getDeductibleReduction
		self.deductible_reduction = if self.deductible_reduction == true
			self.time * 400
		elsif self.deductible_reduction == false
			0
		end
	end

	def getActions
		@actions = [
			{:who => "driver", :type => "debit", :amount => (self.price + self.deductible_reduction).round(0)},
			{:who => "owner", :type => "credit", :amount => (self.price - self.commission).round(0)},
			{:who => "insurance", :type => "credit", :amount => (self.insurance_fee).round(0)},
			{:who => "assistance", :type => "credit", :amount => (self.assistance_fee).round(0)},
			{:who => "drivy", :type => "credit", :amount => (self.drivy_fee + self.deductible_reduction).round(0)}
		]
	end

	private

	def getPricePerDayWithDecrease(price_per_day)
		if self.time.to_i == 1 
			price_per_day
		elsif self.time.to_i < 4  
			price_per_day * (1 + (self.time.to_i - 1) * 0.9)
		elsif self.time.to_i < 10
			price_per_day * (3.7 + (self.time.to_i - 4) * 0.7)
		else
			price_per_day * (7.9 + (self.time.to_i - 10) * 0.5)
		end
	end
end