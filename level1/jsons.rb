class Json
	
	attr_accessor :cars, :rentals, :errors

	ATTRIBUTES = ["cars", "rentals"]

	def initialize(file)
  		json = File.read(file)
  		data_hash = JSON.parse(json)
  		@cars = data_hash["cars"]
  		@rentals = data_hash["rentals"]
  		@errors = []
  		checkJsonErrors
	end

	def createOutputJson(contents)
		File.open("output.json","w"){|f| f.write(JSON.pretty_generate(contents))}
	end

	private

	def checkJsonErrors
		ATTRIBUTES.each do |att|
			if self.send(att) == nil
				@errors.push("table #{att} is null")
			end
		end
	end

end