class Rental

	attr_accessor :id, :car_id, :time, :distance, :price, :start_date, :end_date

	def initialize(id, car_id, distance, start_date, end_date)
		@id = id
		@car_id = car_id
		@distance = distance
		@start_date = start_date
		@end_date = end_date
		@time = getTime
	end

	def getTime
		(Time.parse(self.end_date).to_i - Time.parse(self.start_date).to_i) / 24 / 3600 + 1
	end

	def getPrice(cars)
		cars.each do |c|
			if self.car_id == c["id"]
				@price = self.time.to_i * c["price_per_day"] + self.distance.to_i * c["price_per_km"]
			end
		end
	end

end
