require 'time'
require 'json'
require './rentals.rb'
require './jsons.rb'
require './application.rb'

datas = Json.new('data.json')
if datas.errors == []
	application = Application.new(datas)
	if application.errors == [] && application.rentals_after_treatment_and_modifications == nil
		datas.createOutputJson({:rentals => application.rentals_after_treatment})
	elsif application.errors != [] && application.rentals_after_treatment_and_modifications == nil
		datas.createOutputJson({:rentals => application.rentals_after_treatment,
			        	  		:errors => application.errors})
	elsif application.errors == [] && application.rentals_after_treatment_and_modifications != nil
		datas.createOutputJson({:rentals => application.rentals_after_treatment,
							   :rent_modifications => application.rentals_deltas})
	elsif application.errors != [] && application.rentals_after_treatment_and_modifications != nil
		datas.createOutputJson({:rentals => application.rentals_after_treatment,
							   :rent_modifications => application.rentals_deltas,
							   :errors => application.errors})
	end
else
	datas.createOutputJson({:errors => datas.errors})
end
