class Application

	attr_accessor :errors, :rentals_after_treatment, :rentals_after_treatment_and_modifications,
				  :rentals_deltas

	def initialize(datas)
		@errors = []
		@rentals_deltas = []
		@cars = checkErrors(datas.cars, "Car")
		@rentals_before_treatment = checkErrors(datas.rentals, "Rental")
		@rentals_after_treatment = rentals(false)
		if datas.rental_modifications != nil
			@rental_modifications = checkErrors(datas.rental_modifications, "Rental_modification")
			@rentals_after_treatment_and_modifications = rentals(true)
			actionsModifications
		end
	end

	def rentals modify
		rentals = []
		@rentals_before_treatment.each do |rent|
			rental = Rental.new(rent["id"], 
								rent["car_id"], 
								rent["distance"], 
								rent["start_date"], 
								rent["end_date"],
								rent["deductible_reduction"])
			if modify == true
				rental = modif(rental)
			end
			rental = setAttributes(rental)
			if modify == false
				rentals.push({:id => rental.id,
						      :actions => rental.actions
						     })
			elsif modify == true && rental.update == true
				rentals.push({:id => rental.modif_id,
							  :rental_id => rental.id,
						      :actions => rental.actions
						     })
			end
		end
		rentals
	end

	def checkErrors(table, klass)
		checked = []
		errors_counter = 0
		table.each do |row|
			row.each do |key, value|
				if value == nil
					errors_counter += 1
					@errors.push("#{key} is null for #{klass} with id #{row['id']}")
				end
			end
			if errors_counter == 0
				checked.push(row)
			end
			errors_counter = 0
		end
		instance_variable_set("@#{klass}", checked)
	end

	def actionsModifications
		@rentals_after_treatment_and_modifications.each do |rent_modif|
			actions = []
			@rentals_after_treatment.each do |rent|
				if rent[:id] == rent_modif[:rental_id]
					rent[:actions].each do |rent_actions|
						rent_modif[:actions].each do |rent_modif_actions|
							if rent_actions[:who] == rent_modif_actions[:who]
								rent_modif_actions[:amount] = rent_modif_actions[:amount] - rent_actions[:amount]
								rent_modif_actions[:type] = getActionType(rent_modif_actions[:amount], rent_modif_actions[:who])
								actions.push({:who => rent_modif_actions[:who], 
								 :type => rent_modif_actions[:type],
								 :amount => rent_modif_actions[:amount] > 0 ? rent_modif_actions[:amount] : -rent_modif_actions[:amount]})
							end
						end
					end
				end
			end
			@rentals_deltas.push({:id => rent_modif[:id],
				:rental_id => rent_modif[:rental_id],
				:actions => actions})
		end
	end

	private

	def modif(rental)
		@rental_modifications.each do |modif|
			if modif["rental_id"] == rental.id
				rental.modification(modif)
			end
		end
		rental
	end

	def setAttributes(rental)
		rental.getTime
		rental.getPrice(@cars)
		if rental.price != nil
			rental.getCommissions
			rental.getDeductibleReduction
			rental.getActions
		end
		rental
	end

	def getActionType(amount, who)
		type = if ( amount < 0 && who == "driver" ) || ( amount > 0 && who != "driver" )
			"credit"
		else
			"debit"
		end
		type
	end

end