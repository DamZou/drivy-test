class Rental

	attr_accessor :id, :car_id, :time, :distance, :price, :start_date, :end_date,
				  :insurance_fee, :assistance_fee, :drivy_fee

	def initialize(id, car_id, distance, start_date, end_date)
		@id = id
		@car_id = car_id
		@distance = distance
		@start_date = start_date
		@end_date = end_date
		@time = getTime
	end

	def getTime
		(Time.parse(self.end_date).to_i - Time.parse(self.start_date).to_i) / 24 / 3600 + 1
	end

	def getPrice(cars)
		cars.each do |c|
			if self.car_id == c["id"]
				@price = getPricePerDayWithDecrease(c["price_per_day"]).round(0) + self.distance.to_i * c["price_per_km"]
			end
		end
	end

	def getCommissions
		if self.price != nil
			commission = self.price * 0.3
			self.insurance_fee = (commission * 0.5).round(0)
			self.assistance_fee = (self.time * 100).round(0)
			self.drivy_fee = (commission - self.insurance_fee - self.assistance_fee).round(0)
		end
	end

	private

	def getPricePerDayWithDecrease(price_per_day)
		if self.time.to_i == 1 
			price_per_day
		elsif self.time.to_i < 4  
			price_per_day * (1 + (self.time.to_i - 1) * 0.9)
		elsif self.time.to_i < 10
			price_per_day * (3.7 + (self.time.to_i - 4) * 0.7)
		else
			price_per_day * (7.9 + (self.time.to_i - 10) * 0.5)
		end
	end
end