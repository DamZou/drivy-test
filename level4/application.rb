class Application

	attr_accessor :errors, :rentals_after_treatment

	def initialize(datas)
		@errors = []
		@cars = checkErrors(datas.cars, "Car")
		@rentals_before_treatment = checkErrors(datas.rentals, "Rental")
		@rentals_after_treatment = rentals
	end

	def rentals
		rentals = []
		@rentals_before_treatment.each do |rent|
			rental = Rental.new(rent["id"], 
								rent["car_id"], 
								rent["distance"], 
								rent["start_date"], 
								rent["end_date"],
								rent["deductible_reduction"])
			rental.getPrice(@cars)
			rental.getCommissions
			rental.getDeductibleReduction
			rentals.push({:id => rental.id,
			    		  :price => rental.price,
			    		  :options => {:deductible_reduction => rental.deductible_reduction},
			    		  :commission => {:insurance_fee => rental.insurance_fee,
			    		  				  :assistance_fee => rental.assistance_fee,
			    		  				  :drivy_fee => rental.drivy_fee}
			    		  })
		end
		rentals
	end

	def checkErrors(table, klass)
		checked = []
		errors_counter = 0
		table.each do |row|
			row.each do |key, value|
				if value == nil
					errors_counter += 1
					@errors.push("#{key} is null for #{klass} with id #{row['id']}")
				end
			end
			if errors_counter == 0
				checked.push(row)
			end
			errors_counter = 0
		end
		instance_variable_set("@#{klass}", checked)
	end

end