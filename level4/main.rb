require 'time'
require 'json'
require './rentals.rb'
require './jsons.rb'
require './application.rb'

datas = Json.new('data.json')
if datas.errors == []
	application = Application.new(datas)
	if application.errors == []
		datas.createOutputJson({:rentals => application.rentals_after_treatment})
	else
		datas.createOutputJson({:rentals => application.rentals_after_treatment,
						       :errors => application.errors})
	end
else
	datas.createOutputJson({:errors => datas.errors})
end
